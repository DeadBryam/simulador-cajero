/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author deadbryam
 */
public class CuentaModelo {

    private String id;
    private String idUsuario;
    private double saldo;

    public CuentaModelo(String id, String idUsuario, double saldo) {
        this.id = id;
        this.idUsuario = idUsuario;
        this.saldo = saldo;
    }

    public CuentaModelo() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

}
