/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.CuentaModelo;

/**
 *
 * @author deadbryam
 */
public class CuentaControlador {

    private final Conexion conexion;
    private ResultSet rst;

    public CuentaControlador() {
        conexion = new Conexion();
    }

    /**
     * Este metodo sirve para buscar una cuenta por el id del usuario.
     *
     * @param idUsuario Id del usuario a buscar.
     * @return Una instancia tipo modelo de cuenta.
     */
    public CuentaModelo cuentaPorUsuario(String idUsuario) {
        rst = conexion.obtener(String.format("SELECT * FROM cuenta WHERE id_usuario = \"%s\" ", idUsuario));
        CuentaModelo modelo = null;
        try {
            rst.next();
            modelo = convertir(rst);
        } catch (SQLException ex) {
            Logger.getLogger(LoginControlador.class.getName()).log(Level.SEVERE, null, ex);
        }

        return modelo;
    }

    /**
     * Este metodo convierte un resultSet tipo consulta a un objeto de modelo
     * consulta
     *
     * @param set ResultSet tipo consulta
     * @return Objeto de tipo modelo consulta
     * @throws SQLException
     */
    private CuentaModelo convertir(ResultSet set) throws SQLException {
        return new CuentaModelo(set.getString("id_cuenta"), set.getString("id_usuario"), set.getDouble("saldo"));
    }

    public void agregarACuenta(double cantidad, String idCuenta) {
        System.out.println(String.format(Locale.US, "UPDATE cuenta SET saldo= %.2f WHERE id_cuenta= \"%s\";", cantidad + 0.12, idCuenta));
        conexion.editar(String.format(Locale.US, "UPDATE cuenta SET saldo= %.2f WHERE id_cuenta= \"%s\";", cantidad, idCuenta),
                "Cantidad agregada correctamente.");
    }

    public void retirarDeCuenta(double cantidad, String idCuenta) {
        conexion.editar(String.format(Locale.US, "UPDATE cuenta SET saldo= %.2f WHERE id_cuenta= \'%s\';", cantidad + 0.12, idCuenta),
                "Cantidad retirada correctamente.");
    }
}
