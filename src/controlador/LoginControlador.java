/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.UsuarioModelo;

/**
 *
 * @author deadbryam
 */
public class LoginControlador {

    Conexion conexion;
    ResultSet rst;

    public LoginControlador() {
        conexion = new Conexion();
    }

    /**
     * Este metodo sirve para saber si un usuario existe en la base o no.
     * @param usuario El nickname o sobre nombre del usuario.
     * @param constrasenia La contraseña del usuario.
     * @return Retorna un booleano para saber si el usuario buscado existe o no.
     */
    public boolean iniciarSesion(String usuario, String constrasenia) {
        rst = conexion.obtener(String.format("SELECT COUNT(*) FROM usuario WHERE usuario = \"%s\" AND contrasenia = \"%s\"", usuario, constrasenia));
        boolean resultado = false;
        try {
            rst.next();
            resultado = rst.getInt(1) != 0;
        } catch (SQLException ex) {
            Logger.getLogger(LoginControlador.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultado;
    }

    /**
     * Este metodo sirve para buscar un usuario or su nickname y contraseña.
     * @param usuario El nickname o sobre nombre del usuario.
     * @param constrasenia La contraseña del usuario.
     * @return Un objeto de tipo usuario.
     */
    public UsuarioModelo busquedaUsuario(String usuario, String constrasenia) {
        rst = conexion.obtener(String.format("SELECT * FROM usuario WHERE usuario = \"%s\" AND contrasenia = \"%s\"", usuario, constrasenia));
        UsuarioModelo modelo = null;
        try {
            rst.next();
            modelo = convertir(rst);
        } catch (SQLException ex) {
            Logger.getLogger(LoginControlador.class.getName()).log(Level.SEVERE, null, ex);
        }

        return modelo;
    }

    /**
     * Este metodo convierte un resultSet tipo consulta a un objeto de modelo
     * consulta
     *
     * @param set ResultSet tipo consulta
     * @return Objeto de tipo modelo consulta
     * @throws SQLException
     */
    private UsuarioModelo convertir(ResultSet set) throws SQLException {
        return new UsuarioModelo(set.getString("id_usuario"), set.getString("nombre"), set.getString("apellido"), set.getString("usuario"));
    }

}
