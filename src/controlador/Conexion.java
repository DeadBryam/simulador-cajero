/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

/**
 *
 * @author deadbryam
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Conexion {

    private final String BASE_URL = "jdbc:mysql://sql3.freemysqlhosting.net/sql3348263";
    private final String USER = "sql3348263";
    private final String PASSWORD = "QSVeYspSXM";
    private Connection cnx = null;
    private Statement sttm = null;
    private ResultSet rst = null;

    public Conexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cnx = DriverManager.getConnection(BASE_URL, USER, PASSWORD);
        } catch (ClassNotFoundException c) {
            JOptionPane.showMessageDialog(null, "ERROR: " + c.getMessage());
            System.exit(1);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e.getMessage());
            System.exit(1);
        }
    }

    /**
     * Este metodo sirve para insertar registros a una base de datos
     *
     * @param sql Una consulta SQL
     */
    public void insertar(String sql) {
        ejecutarConsulta(sql, "Regsitro agregado correctamente");
    }

    /**
     * Este metodo sirve para editar registros de una base de datos
     *
     * @param sql Una consulta SQL
     */
    public void editar(String sql) {
        ejecutarConsulta(sql, "Registro editado correctamente");
    }

    /**
     * Este metodo sirve para editar registros de una base de datos
     *
     * @param sql Una consulta SQL.
     * @param mensaje Un mensaje personalizado.
     */
    public void editar(String sql, String mensaje) {
        ejecutarConsulta(sql, mensaje);
    }

    /**
     * Este metodo sirve para elimnar registros de una base de datos
     *
     * @param sql Una consulta SQL
     */
    public void eliminar(String sql) {
        ejecutarConsulta(sql, "Registro eliminado correctamente");
    }

    /**
     * Este metodo sirve para obtener registros de una base de datos
     *
     * @param sql Una consulta SQL
     */
    public ResultSet obtener(String sql) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cnx = DriverManager.getConnection(BASE_URL, USER, PASSWORD);
            sttm = cnx.createStatement();
            rst = sttm.executeQuery(sql);
        } catch (ClassNotFoundException c) {
            JOptionPane.showMessageDialog(null, "ERROR: " + c.getMessage());
            System.exit(1);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e.getMessage());
            System.exit(1);
        }
        return rst;

    }

    private void ejecutarConsulta(String sql, String mensaje) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cnx = DriverManager.getConnection(BASE_URL, USER, PASSWORD);
            sttm = cnx.createStatement();
            sttm.executeUpdate(sql);
            JOptionPane.showMessageDialog(null, mensaje);
        } catch (ClassNotFoundException c) {
            JOptionPane.showMessageDialog(null, "ERROR: " + c.getMessage());
            System.exit(1);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "ERROR: " + e.getMessage());
            System.exit(1);
        }
    }

    public void cerrar() {
        try {
            this.cnx.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Connection getConexion() {
        return this.cnx;
    }
}
